"""
Analyse the results of multiple UV-VIS titrations.
Read data using a specific folder structure (see functions below) and
perform a non-linear fit for each data set. Output the resulting K_D value
for each experiment as well as relevant figures.
When called as a standalone script: run evaluation using default parameters.
"""
import os
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np
from scipy import optimize, stats
import traceback
FIG = 1


class UVExperiment:
    """
    Class representing one titration experiment.

    Instance variables:
    _receptor (str) -- receptor used for the experiment
    _ligand (str) -- ligand used for the experiment
    _path (str) -- path to the data files
    _all_wavelengths (int[]) -- all wavelengths measured during the experiment
                                (assumed to be the same at all concentrations)
    _data (dict{float: float[]}) -- raw experimental data
    _conc_arr -- concentrations measured
    _wavelengths (int[]) -- wavelengths used for analysis
    _curves (dict{int: np.ndarray(float)}) -- binding curves at each wavelength
                                              defined in _wavelengths
    _K_D (float[]) -- K_D values determined for each wavelength in _wavelength
    _fits (dict{int}: np.array) -- fitted parameters

    Class variables:
    DEFAULT_WAVELENGTHS (int[]) -- default wavelengths to analyse
    FORMAT_METHOD (str) -- method used to show error of data ('CI' or 'SEM')

    Public methods:
    evaluate -- perform full evaluation
    read_data -- read data files
    fit_binding -- fit binding curve to experimental data
    plot_spectra -- plot all experimental data to one file
    plot_fits -- plot fitted binding curves at all chosen wavelengths
    save_fits -- save K_D values to text file
    save_data -- save raw experimental data to combined .csv file
    get_wavelengths -- getter for wavelengths array
    set_wavelengths -- setter for wavelengths array
    get_receptor -- getter for receptor name
    get_ligand -- getter for ligand name

    Class methods:
    read_csv -- read CSV file for a single experiment
    binding -- calculate binding curve
    resid -- calculate residual of binding curve
    formatCI -- get nicely formatted mean & 95% confidence interval of data
    formatSD -- get nicely formatted mean & SEM of data
    format_data -- call either formatCI or formatSD
    """

    DEFAULT_WAVELENGTHS = [250, 254, 258, 262, 266]
    FORMAT_METHOD = 'CI'

    def __init__(self, receptor_="U1,2", ligand_="Lactulose", data_path=None):
        """
        Save instance variables, find experimental data files & read in data.

        Saves arguments to _receptor and _ligand, then defines _path
        from these. Looks for files within _path and throws an error
        if the necessary folder does not yet exist or if it is empty.

        Arguments:
        receptor_ (str) -- name of receptor
        ligand_ (str) -- name of ligand
        data_path (str) -- alternative path to experimental data
        """
        # Set relevant variables
        self._receptor = receptor_
        self._ligand = ligand_
        if data_path is None:
            self._path = f'./data/{self._receptor}/{self._ligand}'
        else:
            self._path = data_path

        try:
            # Get list of files with experimental data
            self._files = [f for f in os.listdir(self._path)
                           if '.csv' in f]
        except FileNotFoundError:
            # Data not yet available
            raise FileNotFoundError('No experimental data found for '
                                    + f'{self._receptor} + {self._ligand}. '
                                    + 'Folder does not seem to exist.')
        else:
            # Handle empty folder
            if len(self._files) == 0:
                raise FileNotFoundError(
                    'No experimental data found for '
                    + f'{self._receptor} + {self._ligand}. '
                    + 'Folder exists, but no CSV files found.'
                    )

    # End of __init__

    def evaluate(self):
        """
        Evaluate this experiment using the standard method.
        Read the data from the path specified at __init__, plot the raw data
        and save to a combined .csv file, fit the binding function to the data,
        plot each fit and save the resulting values to an .out file.
        """
        self.read_data(self._path)
        self.plot_spectra()
        self.save_data()
        self.fit_binding()
        self.plot_fits()
        self.save_fits()

    def read_data(self, data_path=None):
        """
        Read experimental data into instance.

        Searches the directory passed or the directory at self._path
        for experimental data. Reads the data files and creates the instance
        variables _all_wavelengths, _data and _conc_arr.

        Parameters:
        data_path (str) -- alternative path to data files
        """
        if data_path is None:
            data_path = self._path

        # Get array of wavelengths measured
        wl_raw = UVExperiment.read_csv(
                                f'{self._path}/{self._files[0][:-4]}.csv', 0
                                )[1]
        self._all_wavelengths = [int(wl) for wl in wl_raw]

        # Read absorbance from all raw data files
        # Previously generated files with combined data contain "comb" as
        # marker
        self._data = dict()
        self._conc_arr = []
        for file in self._files:
            if (".csv" in file) and ("comb" not in file) and \
                    ("ignore" not in file):
                conc_f, abs_f = UVExperiment.read_csv(f'{self._path}/{file}',
                                                      1)
                self._data[conc_f] = abs_f
                self._conc_arr.append(conc_f)

        return self._data
    # End of read_data

    def fit_binding(self, wl_path=None):
        """
        Fits binding curve to experimental data, using wavelengths file.

        Uses scipy.optimise.least_squares to fit a "total binding curve"
        (as used by Prism) to the experimental data at the wavelengths
        given in wavelengths.arg (either in wl_path or self._path)

        Arguments:
        wl_path (str) -- alternative path to wavelengths.arg
        """
        if wl_path is None:
            # Use default value (could not pass as argument due to "self")
            wl_path = f'{self._path}/wavelengths.arg'
        try:
            # Find wavelengths file
            with open(wl_path, 'r') as wl_handle:
                self._wavelengths = [int(wl) for wl in
                                     wl_handle.readline().split(',')]
        except FileNotFoundError:
            # Use default wavelengths & save file with default list
            self._wavelengths = UVExperiment.DEFAULT_WAVELENGTHS
            wl_str = ''
            for wl in self._wavelengths:
                wl_str = wl_str + str(wl) + ','
            with open(wl_path, 'w', encoding='utf-8') as wl_handle:
                wl_handle.write(wl_str[:-1])
        else:
            if len(self._wavelengths) > 5:
                raise ValueError(
                    'Too many wavelengths specified in wavelengths.arg. '
                    + f'{len(self._wavelengths)} wavelengths specified, '
                    + 'maximum possible number is 5.'
                                )

        # Save binding curves to instance variable _curves
        self._curves = dict()
        for wl in self._wavelengths:
            self._curves[wl] = []
            idx = self._all_wavelengths.index(wl)
            for conc in self._data:
                self._curves[wl] = np.append(self._curves[wl],
                                             self._data[conc][idx])

        K_D_arr = []
        self._fits = dict()
        for wl in self._curves:
            # Fit experimental data to binding model
            # Reminder: params = [A_max, K_D (mM), NS, BG]
            opt = optimize.least_squares(
                    UVExperiment.resid,
                    [max(self._curves[wl]), 1.0e-3, 1.0e-8, 1.0e-8],
                    bounds=(np.array([-np.inf, 1.0e-6, -np.inf, -np.inf]),
                            np.array([np.inf, np.inf, np.inf, np.inf])),
                    args=(np.array(self._conc_arr[1:]),
                          np.array(self._curves[wl][1:]))
                    )

            # Save K_D estimate to array, add to string for outputting
            fit = opt.x
            K_D_single = fit[1]/1000
            K_D_arr.append(K_D_single)
            self._fits[wl] = fit

        self._K_D = np.array(K_D_arr)
        return self._fits
    # End of fit_binding

    def plot_spectra(self, out_path=None):
        """
        Plot absorbance vs. wavelength with all concentrations in one plot.

        Save figure under <pep>/<sugar>/<pep>_<sugar>_spectra.png
        """
        if out_path is None:
            out_path = self._path
        global FIG
        artists = []
        legTxt = []
        plt.figure(FIG)
        plt.xlim(240, 300)
        plt.ylim(min([min(self._data[c]) for c in self._data]) - 0.05,
                 max([max(self._data[c]) for c in self._data]) + 0.05)
        plt.xlabel(r"Wavelength $\lambda$ / nm")
        plt.ylabel("Absorption")
        colours = cm.rainbow(np.linspace(0, 1, len(self._data)))
        for conc, col in zip(sorted(self._data), colours):
            art, = plt.plot(self._all_wavelengths, self._data[conc], lw=0.5,
                            c=col)
            artists.append(art)
            legTxt.append(f'{conc} mM')
        plt.legend(artists, legTxt, framealpha=0.5, fontsize="x-small")
        plt.savefig(f'{out_path}/{self._receptor}_{self._ligand}_spectra.png',
                    dpi=600)
        plt.close(FIG)
        FIG += 1
    # End of plot_spectra

    def plot_fits(self, out_path_1=None, out_path_2=None, show_plot=False):
        """
        Plots binding measurements with fitting curve at given wavelengths.

        Plots 5 graphs containing data points and the calculated binding curve.

        Parameters:
        out_path_1 (str) -- alternative folder to save data (default out/)
        out_path_2 (str) -- alternative folder to save data
                                (default self._path)
        show_plot (bool) -- toggles plt.show() after generating figures
        """
        # Use default paths
        if out_path_1 is None:
            out_path_1 = 'out'
        if out_path_2 is None:
            out_path_2 = self._path

        # Array of concentrations to draw fitted binding curve
        conc_sim = np.linspace(0, max(self._conc_arr))

        # Prepare Matplotlib figure for binding curves
        # Figure contains 5 subplots -> one for each wavelength checked
        fig = plt.figure(constrained_layout=True,
                         figsize=(12, 6))
        axs_shaped = fig.subplots(len(self._curves)//3 + 1, 3)
        axs = axs_shaped.flat
        subfigNr = 0
        plt_text = f'{self._receptor} + {self._ligand}:\n'

        for wl in self._curves:
            # Plot binding curve & exp. data onto subfigure
            fit = self._fits[wl]
            abs_sim = UVExperiment.binding(fit, conc_sim)
            subfig = axs[subfigNr]
            subfig.set_xlim(0, max(self._conc_arr))
            abs_range = max(self._curves[wl]) - min(self._curves[wl])
            subfig.set_ylim(min(self._curves[wl]) - abs_range/10,
                            max(self._curves[wl]) + abs_range/10)
            subfig.set_xlabel("Ligand concentration / mM")
            subfig.set_ylabel(f'Absorption ({int(wl)}nm)')
            subfig.plot(conc_sim, abs_sim)
            subfig.scatter(self._conc_arr[1:], self._curves[wl][1:])
            subfig.scatter(self._conc_arr[0], self._curves[wl][0], marker="x")
            subfigNr += 1
            plt_text += f'{wl}nm: kD = {fit[1]:0.3e}mM\n'

        # Add average K_D to figure & detailled output file
        plt_text += f'Average: kD = {UVExperiment.format_data(self._K_D*1000)}mM'
        plt.text(0, 0, plt_text,
                 horizontalalignment='left',
                 verticalalignment='bottom',
                 transform=axs[3*(len(self._curves)//3 + 1) - 1].transAxes)

        # Finish plot, save to relevant directories
        for i in range(subfigNr, 3*(len(self._curves)//3 + 1)):
            axs[i].axis('off')
        plt.savefig(f'{out_path_1}/{self._receptor}_{self._ligand}_fits.png')
        plt.savefig(f'{out_path_2}/{self._receptor}_{self._ligand}_fits.png')

        if show_plot:
            plt.show()

        plt.close()
    # End of plot_fits

    def save_fits(self, out_path=None):
        """
        Save calculated K_D values to a text file.

        Arguments:
        out_path (str) -- alternative output path (default self._path)
        """
        if out_path is None:
            out_path = self._path
        out_str = f'{self._receptor} + {self._ligand}:\n'

        for i in range(len(self._K_D)):
            out_str += (f'{self._wavelengths[i]}nm:\tK_D = '
                        + f'{self._K_D[i]:0.3e} M\n')

        out_str += f'Average:\tK_D = {UVExperiment.format_data(self._K_D)} M'

        with open(f'{self._path}/{self._receptor}_{self._ligand}_out.txt',
                  'w', encoding='utf-8') as out_handle:
            out_handle.write(out_str)
    # End of save_fits

    def save_data(self):
        """
        Save all experimental data to one CSV file.

        (e.g. for using in Prism later)
        Header line: "wavelength,<conc1>,<conc2>,..."
        """
        try:
            handle = open(
                f'{self._path}/{self._receptor}_{self._ligand}_comb.csv',
                'w',
                encoding='utf-8'
                )
        except FileNotFoundError:
            return 0
        else:
            line = "wavelength,"
            for conc in sorted(self._data):
                line += str(conc) + ','
            handle.write(line[:-1] + '\n')

            for i in range(len(self._data[0])):
                line = str(self._all_wavelengths[i]) + ','
                for conc in sorted(self._data):
                    line += str(self._data[conc][i]) + ','
                handle.write(line[:-1] + '\n')

            handle.close()
            return 1
    # End of save_data

    def get_wavelengths(self):
        """ Return wavelengths to be analysed for this experiment as array. """
        return self._wavelengths

    def set_wavelengths(self, new_wavelengths):
        """
        Set wavelengths to be analysed for this experiment.

        Arguments:
        wl -- Array of wavelengths to be analysed (as integers)
        """
        self._wavelengths = new_wavelengths

    def get_receptor(self):
        """ Return the name of the receptor used for this experiment. """
        return self._receptor

    def get_ligand(self):
        """ Return the name of the ligand used for this experiment. """
        return self._ligand

    def get_K_D(self):
        """ Return calculated K_D values """
        return self._K_D

    def format_K_D_avg(self):
        """ Return formatted K_D average """
        return UVExperiment.format_data(self._K_D*1000)

    @classmethod
    def read_csv(cls, filename, col):
        """
        Read CSV file containing data from a single titration experiment.

        Arguments:
        filename -- name of CSV file to be read
        col -- column of CSV file to return
                (0 for wavelengths, 1 for absorbance)

        Returns:
        conc (float) -- concentration used in measurement,
                        read at top left of file
        val (float[]) -- contents of column defined in col
        """
        with open(filename, 'r') as handle:
            conc = handle.readline().split("mM")[0]
            val = []
            handle.readline()
            for line in handle:
                line_arr = line.split(",")
                if len(line_arr) == 3:
                    val.append(float(line_arr[col]))
        return float(conc), val
    # End of read_csv

    @classmethod
    def binding(cls, params, conc):
        """
        Return a binding curve dependent on "conc".

        Use parameters defined in array "params"
        params = [A_max, K_D (mM), NS, BG]
        """
        return params[0]*conc/(params[1]+conc) + params[2]*conc + params[3]

    @classmethod
    def resid(cls, params, conc, a):
        """
        Calculate residuals of fit to binding curve

        params = [A_max, K_D (mM), NS, BG]
        conc: ligand concentrations used
        a: measured absorbance values
        """
        return cls.binding(params, conc) - a

    @classmethod
    def formatCI(cls, data, verbose=False):
        """ Get average & 95% conf. interval of array & format nicely """
        # Get mean, standard error of mean & t distribution CDF
        # -> conf. interval
        avg_raw = np.mean(data)
        sem = stats.sem(data)
        t = stats.t.ppf(q=0.975, df=len(data)-1)
        ci_raw = sem*t

        # Use rule of 25 for confidence interval
        ci_exp = int(np.log10(ci_raw))
        if ci_exp < 0:
            ci_exp -= 1
        ci_man = ci_raw/(10**ci_exp)
        prec = -ci_exp
        if ci_man < 2.5:
            prec += 1
        ci = round(ci_raw, prec)

        # Use rule of 25 for average
        avg_exp = int(np.log10(avg_raw))
        if avg_exp < 0:
            avg_exp -= 1
        if avg_exp >= ci_exp:
            avg = round(avg_raw, prec)
        else:
            avg = round(avg_raw, -avg_exp)

        if verbose:
            # Print variables to console for debugging
            print(f'Raw: {avg_raw} ± {ci_raw}')
            print(f'Exponents: {avg_exp}, {ci_exp}')
            print(f'Mantissa: {ci_man}')
            print(f'-> Precision: {prec}')

        # Output formatted value as f-string
        try:
            return f'{avg:0.{avg_exp + prec}e} ± {ci:0.{ci_exp + prec}e}'
        except ValueError:
            return f'{avg:0.1e} ± {ci:0.1e}'

    @classmethod
    def formatSD(cls, data, verbose=False):
        """ Get average & SEM of array & format nicely """
        # TODO: Debug!
        # Get mean & standard error of mean
        avg_raw = np.mean(data)
        sem_raw = stats.sem(data)

        # Use rule of 25 for confidence interval
        sem_exp = int(np.log10(sem_raw))
        if sem_exp < 0:
            sem_exp -= 1
        sem_man = sem_raw/(10**sem_exp)
        prec = -sem_exp
        if sem_man < 2.5:
            under25 = True
            prec += 1
        else:
            under25 = False
        sem = round(sem_raw, prec)

        # Use rule of 25 for average
        avg_exp = int(np.log10(avg_raw))
        if avg_exp < 0:
            avg_exp -= 1
        if avg_exp >= sem_exp:
            avg = round(avg_raw, prec)
        else:
            avg = round(avg_raw, -avg_exp)

        if verbose:
            # Print variables to console for debugging
            print(f'Raw: {avg_raw} ± {sem_raw}')
            print(f'Exponents: {avg_exp}, {sem_exp}')
            print(f'Mantissa: {sem_man}')
            print(f'-> Precision: {prec}')

        # Output formatted value as f-string
        try:
            if under25:
                return f'{avg:0.{avg_exp + prec}e}({int(sem_man*10)})'
            else:
                return f'{avg:0.{avg_exp + prec}e}({int(sem_man)})'
        except ValueError:
            return f'{avg:0.1e}({sem:0.1e})'

    @classmethod
    def format_data(cls, data, method=None, verbose=False):
        """ Call either formatCI or formatSD depending on "method" """
        if method is None:
            method = cls.FORMAT_METHOD
        if method == 'CI':
            return cls.formatCI(data, verbose)
        elif method == 'SEM' or method == 'SD':
            return cls.formatSD(data, verbose)
        else:
            raise ValueError(f'format_data: method "{method}" unknown')
# End of class UV_Experiment


class UVAnalysis:
    """
    Class for analysis of entire UV-VIS titration data set.

    Instance variables:
    _receptors -- array of all receptors used
    _ligands -- array of all ligands used
    _log_file -- path to log file (default: 'uv_vis.log')
    _out_file -- path to summary file (default: 'UV-VIS_analysis.out')
    _data_dir -- directory containing data files

    Class variables:
    RECEPTORS_DEFAULT -- default array of receptors (peptides)
    LIGANDS_DEFAULT -- default array of ligands (sugars)

    Instance methods:
    evaluate() -- perform evaluation of all experiments
    """
    RECEPTORS_DEFAULT = ['U2,3', 'U2,4', 'U1,2', 'U2,6', '2S4R', '2S']
    LIGANDS_DEFAULT = ['Mannose', 'Glucose', 'Galactose', 'Fructose',
                       'Trehalose', 'Trehalose-2', 'Trehalose-2A',
                       'Trehalose-2B', 'Trehalose-2C', 'Ribose', 'Raffinose',
                       'Lactulose', 'Maltose', 'Sucrose', 'Sucrose-2',
                       'Sucrose-2A', 'Sucrose-2B', 'Sucrose-2C', 'Lactose']

    def __init__(self, receptors=None, ligands=None,
                 log_file='uv_vis.log',
                 out_file='UV-VIS_analysis.out',
                 data_dir=None):
        """
        Create instance using arguments to initialise instance variables

        Arguments:
        receptors -- list of receptors to analyse
        ligands -- list of ligands to analyse
        log_file -- name of file to log output to
        out_file -- name of file to output summary to
        data_dir -- path to directory containing experimental data
        """
        if receptors is None:
            self._receptors = UVAnalysis.RECEPTORS_DEFAULT
        else:
            self._receptors = receptors
        if ligands is None:
            self._ligands = UVAnalysis.LIGANDS_DEFAULT
        else:
            self._ligands = ligands

        self._log_file = log_file
        self._out_file = out_file

        self._data_dir = data_dir

    def evaluate(self):
        """
        Evaluate results of all experiments.

        Loop through self._receptors and self._ligands, search for the
        respective data file and analyse using the UVExperiment class.
        Log any output generated to self._log_file and output a summary
        to self._out_file.
        """
        out_str = ''
        log_handle = open(self._log_file, 'w', encoding='utf-8')

        for re in self._receptors:
            for li in self._ligands:

                print(f'Analysing {re} + {li}...')
                log_handle.write(f'Analysing {re} + {li}...\n')

                try:
                    if self._data_dir is None:
                        exp = UVExperiment(re, li)
                    else:
                        exp_data_dir = f'{self._data_dir}/{re}/{li}'
                        exp = UVExperiment(re, li, data_path=exp_data_dir)
                    exp.evaluate()

                except FileNotFoundError as e:
                    print(e)
                    log_handle.write(str(e))
                    log_handle.write('\n')

                except ValueError as e:
                    print(e)
                    log_handle.write(str(e))
                    log_handle.write('\n')

                except Exception as e:
                    print(f'ERROR: {str(e)}. See log file for traceback.')
                    log_handle.write(f'ERROR: {str(e)}\n')
                    log_handle.write(traceback.format_exc() + '\n')
                    log_handle.write('\n')
                    raise e

                else:
                    out_str += f'{re} + {li}: {exp.format_K_D_avg()}mM\n'
                    print(f'{re} + {li} analysed successfully.')
                    log_handle.write(f'{re} + {li} analysed successfully.\n')

                finally:
                    print()
                    log_handle.write('\n')

            # End of "for sugar in sugars"
            out_str += '\n'
        # End of "for pep in peptides"

        with open(self._out_file, 'w', encoding='utf-8') as out_handle:
            out_handle.write(out_str)

        log_handle.write('Output written succesfully.\n')
        log_handle.write('Closing...')
        log_handle.close()
    # End of evaluate()
# End of class UVAnalysis


# If running directly
if __name__ == '__main__':
    ana = UVAnalysis()
    ana.evaluate()
