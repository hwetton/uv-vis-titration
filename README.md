# UV-VIS Titration

Automatic evaluation of UV/VIS titration data for multiple experiments.

**Author:** Henry Wetton\
**Author Email:** hwetton@student.ethz.ch\
**Version:** 1.0\
**Release Date:** 2021-11-24

## Prerequisites

- Python 3 (created for Python 3.9.7)
- Python packages/modules:
    - `os`
    - `numpy`
    - `traceback`
    - `matplotlib` (for `matplotlib.pyplot` and `matplotlib.cm`)
    - `scipy` (for `scipy.optimize` and `scipy.stats`)

## Installation

1. Install [Python 3](https://www.python.org/downloads/) (should automatically include `pip`).
2. Using pip, install the modules listed above by running `pip install <module>` from a terminal.
3. Save the file `uv_vis.py` to the root file of your data.
4. Ensure the file structure is correct: The folder containing `uv_vis.py` should contain one folder named `out/` and one folder named `data/`.
5. Ensure correct data file format. Data should be stored in `.csv` files in the following format:
    - First line: `<conc>mM <ligand>,` (`<conc>`: ligand concentration; comma at end required!)
    - Second line: `Wavelength, Abs`
    - Third line: First line of experimental data
    - This format is automatically produced using the CaryUV Scan program with the Cell Changer enabled -- the sample name input before each measurement becomes the first line of the file.

## Use

1. Carry out experiments and save the data as CSV files in format given above.
2. Place the experimental data into the folder `data/`, using the path `data/<receptor>/<ligand>/<filename>.csv` (the filename is not relevant to the evaluation and can be chosen arbitrarily).
3. Ensure all tested receptors and ligands are contained in the lists on lines 568-573 of `uv_vis.py`.
4. Run the file `uv_vis.py`. The console output shows the progress of the script.
5. View experimental data in `out/` or in `data/<receptor>/<ligand>/`
6. If needed: Create a file named `wavelengths.arg` in `data/<receptor>/<ligand>/` containing only the wavelengths to be analysed, separated by commas. Then run the script again. (This file is generated automatically in the correct format after running the script once; the contained wavelengths can then be modified as needed.) **Important:** A maximum of 5 wavelengths can be analysed at once, otherwise an error occurs.
